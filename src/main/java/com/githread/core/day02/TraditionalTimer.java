package com.githread.core.day02;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by zhubo on 2017/6/14.
 */
public class TraditionalTimer {
    public static void main(String[] args) {

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("bombing!");
            }
        },1000l);
    }

}
